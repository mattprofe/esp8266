// pin del led soldado en la placa
const int led = 5;

void setup() {
  pinMode(led, OUTPUT); // pin como salida
}

void loop() {
  digitalWrite(led, LOW); // apaga el led
  delay(1000);	// espera 1 segundo
  digitalWrite(led, HIGH); // enciende el led
  delay(2000);	// espera 2 segundos
}
