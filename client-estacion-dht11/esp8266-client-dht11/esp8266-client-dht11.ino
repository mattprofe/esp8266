#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <DHT.h> //libreria de tempratura y humedad
#include "esp8266_modes.hpp"
#include "esp8266_send.hpp"

#define DHTTYPE DHT11 // Dependiendo del tipo de sensor
#define DHTPIN 14// Definimos el pin digital donde se conecta el sensor temp y hum 

DHT dht(DHTPIN, DHTTYPE); // Instanciamos el objeto DHT con los datos del tipo y pin

// Nombre y clave de la red Wifi a la que va a conectarse
const char *ssid = "nombre-red";
const char *pass = "contraseña-red";

// Guardará el id de esp8266
String chipid = "";

// dominio al cual nos conectamos
String host = "dominio.com.ar";
// Puerto por el cual accederemos
int port = 81;
// url del archivo al cual enviaremos los datos de los sensores
String url = "/xx/yy/enviardatos.php";

// Almacena el milisegundo cuando se realizo el envio de datos
unsigned long pastMillis = 0;

// variables donde guardamos los valores de los sensores

float temp = 0, tempMin = 99, tempMax = -99;
float hume = 0, humeMin = 99, humeMax = -99;

// Sección de configuración e inicialización del esp8266
// ============================
void setup(){
 
  // Demora para estabilización
  delay(1000);
  
  // Inicializa la comunicación serial
  Serial.begin(115200);
  Serial.println("\r\n[OK] Comunicación Serial.");

  dht.begin();// Comenzamos el sensor DHT
  Serial.println("\r\n[OK] DHT11 iniciado.");

  // optiene el id del ESP8266 y lo conecta a la red wifi
  chipid = wifiSTA(ssid, pass);
  
}

// Bucle infinito
// ============================
void loop(){

  // Si pasaron 60 segundos 
  if(millis() - pastMillis >= 60000){
    // lo presente al pasado
    pastMillis = millis();
    
    // lectura de sensores
    temp = dht.readTemperature();
    hume = dht.readHumidity();

    if(temp>tempMax)
      tempMax = temp;
    if(temp<tempMin)
      tempMin = temp;

    if(hume>humeMax)
      humeMax = hume;
    if(hume<humeMin)
      humeMin = hume;

    // Variables con valores a enviar por POST
    String data = "chipid=" + chipid + "&temperatura=" + String(temp, 2) + "&humedad=" + String(hume, 2) + "&tempmax=" + String(tempMax, 2)+ "&tempmin=" + String(tempMin, 2)+ "&hummax=" + String(humeMax, 2);

    // Envio los datos al host/url por el puerto especificado
    Serial.println(sendDataAPI(data, host, url, port));
 }
}
