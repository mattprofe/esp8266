# client-estacion-dht11

Envio de datos de un sensor DHT11 conectado al ESP8266 a una API que almacena los datos en una DB.

- esp8266-client-dht11.ino
- esp8266_modes.hpp
- esp8266_send.hpp

Autor
=====
- Matias Baez
- @matt_profe
- mbcorp.matias@gmail.com
- https://mattprofe.com.ar