# releX2

El ESP8266 se conecta a la red wifi especificada en el código, muestra por medio del monitor serial la IP obtenida, al colocar la IP en el browser de cualquier dispositivo conectado a la misma red y agregando el puerto 82 (ejemplo: 192.168.1.220:82) veremos una página de bienvenida, luego agregando a la url "/rele1on" o "/rele1off" podremos encender o apagar uno de los 2 reles que tenemos conectados en los pines 5 y 4.

Autor
=====
- Matias Baez
- @matt_profe
- mbcorp.matias@gmail.com
- https://mattprofe.com.ar