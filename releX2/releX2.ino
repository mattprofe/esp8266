#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

const char* ssid = "nombre_red_wifi";
const char* password = "clave_wifi";

const int rele1 = 5;
const int rele2 = 4;

//para led que parpadea
long ledOn = 1500;
long ledOff = 100;
long prevMillis = 0;

//Puerto donde muestra la web
ESP8266WebServer server(82); //!!!!!!!!!!ojooo con el puerto

void setup() {
  Serial.begin(115200);
  delay(10);

  //Configuración  del GPIO2
  pinMode(rele1, OUTPUT);
  pinMode(rele2, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(rele1,LOW);
  digitalWrite(rele2,LOW);
  
  
  Serial.println();
  Serial.println();
  Serial.print("Conectandose a red : ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password); //Conexión a la red
  
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi conectado");

  Serial.println("Ingrese desde un navegador web usando la siguiente IP:");
  Serial.print(WiFi.localIP()); //Obtenemos la IP
  Serial.println(":82");
  
  Serial.println("/rele1on = Enciende el rele1 ; /rele1off = Apaga el rele1");
  Serial.println("/rele1on = Enciende el rele2 ; /rele1off = Apaga el rele2");

  server.on("/", handleRoot);
  server.on("/rele1on", rele1on);
  server.on("/rele1off", rele1off);
  server.on("/rele2on", rele2on);
  server.on("/rele2off", rele2off);
  server.onNotFound(error404);
  
  
  server.begin(); //Iniciamos el servidor
  Serial.println("Servidor Iniciado");
  
}

void loop() {
  server.handleClient();

  long milis = millis();
  
  if(milis>(prevMillis+ledOn+ledOff)){
    prevMillis = milis;
  }

  if(milis>(prevMillis+ledOn)){
    digitalWrite(LED_BUILTIN, LOW);
  }else{
    digitalWrite(LED_BUILTIN, HIGH);
  }


}

void handleRoot(){
  Serial.println("entro en index");
  server.send(200, "text/plain", "Bienvenido");
}

void rele1on(){
  Serial.println("rele1on");
  server.send(200, "text/plain", "Rele1 On");
  digitalWrite(rele1,HIGH);
}

void rele1off(){
  Serial.println("rele1off");
  server.send(200, "text/plain", "Rele1 Off");
  digitalWrite(rele1,LOW);
}

void rele2on(){
  Serial.println("rele2on");
  server.send(200, "text/plain", "Rele2 On");
  digitalWrite(rele2,HIGH);
}

void rele2off(){
  Serial.println("rele2off");
  server.send(200, "text/plain", "Rele2 Off");
  digitalWrite(rele2,LOW);
}

void error404(){
  server.send(200, "text/plain", "No encontrado");
}
